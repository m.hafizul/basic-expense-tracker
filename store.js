import { createStore, applyMiddleware} from 'redux';
// import { persistStore, persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './app/models/root-reducer';
import { handler as expenseSaga } from './app/models/expense/sagas';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
// const persistConfig = {
//     key: 'root',
//     storage,
// };
// const persistedReducer = persistReducer(persistConfig, rootReducer);

sagaMiddleware.run(expenseSaga);

export { store };

// export default () => {
//     const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
//     const persistor = persistStore(store);
//     return { store, persistor };
// };