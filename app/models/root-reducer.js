import { combineReducers } from 'redux';
import { reducer as expenseReducer } from './expense/reducers';

const rootReducer = combineReducers({
    expense: expenseReducer,
});

export default rootReducer;