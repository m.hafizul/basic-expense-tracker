import { put, takeEvery, call, all } from 'redux-saga/effects';
import {
    GET_ALL_EXPENSES_REQUEST,
    GET_ALL_EXPENSES_REQUEST_SUCCESS,
    REMOVE_EXPENSES_REQUEST,
    REMOVE_EXPENSES_REQUEST_SUCCESS,
    REMOVE_EXPENSES_REQUEST_FAILURE,
    STORE_EXPENSES_REQUEST,
    STORE_EXPENSES_REQUEST_FAILURE,
    STORE_EXPENSES_REQUEST_SUCCESS,
    EXTERNAL_TEST_REQUEST,
    EXTERNAL_TEST_REQUEST_SUCCESS,
    EXTERNAL_TEST_REQUEST_FAILURE
} from './actionConstant';

function* handler() {
    yield takeEvery(STORE_EXPENSES_REQUEST, storeNewExpense);
    yield takeEvery(REMOVE_EXPENSES_REQUEST, removeExpense);
    yield takeEvery(EXTERNAL_TEST_REQUEST, testApiCall);
}

function* storeNewExpense(action) {
    try {
        yield put({ type: STORE_EXPENSES_REQUEST_SUCCESS, payload: action.payload });
    } catch (err) {
        // Do nothing atm
    }
}


function* removeExpense(action) {
    try {
        yield put({ type: REMOVE_EXPENSES_REQUEST_SUCCESS, payload: action.payload });
    } catch (err) {
        // Do nothing atm      
    }
}

function* testApiCall() {
    try {
        const data = yield call(fetchData);
        yield put({ type: EXTERNAL_TEST_REQUEST_SUCCESS, serverExpensesList: data });
    } catch (err) {
        yield put({ type: EXTERNAL_TEST_REQUEST_FAILURE, message: err.message });
    }
}

const fetchData = async () => {
    try {
      const response = await fetch("https://chronous.tech/api/getExpenses");
      const data = await response.json();
      console.log('data' ,data);
      return data;
    } catch (e) {
      console.log(e);
    }
};

export { handler };