import { STORE_EXPENSES_REQUEST, REMOVE_EXPENSES_REQUEST, EXTERNAL_TEST_REQUEST } from "./actionConstant";

export const addNewExpenses = (content) => ({
    type: STORE_EXPENSES_REQUEST,
    payload: {
        desc : content.name,
        cat  : content.category,
        amnt : content.amount
    }
});

export const removeExpenses = (content) => ({
    type: REMOVE_EXPENSES_REQUEST,
    payload: content
});

export const testApiCall = () => ({
    type: EXTERNAL_TEST_REQUEST
});