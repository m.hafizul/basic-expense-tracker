import uuid from 'react-native-uuid';
import { ToastAndroid } from "react-native";
import { 
    GET_ALL_EXPENSES_REQUEST_SUCCESS, 
    STORE_EXPENSES_REQUEST_SUCCESS, 
    REMOVE_EXPENSES_REQUEST_SUCCESS,
    EXTERNAL_TEST_REQUEST_SUCCESS,
    EXTERNAL_TEST_REQUEST_FAILURE
} from "./actionConstant";

const showNewExpenseToast = (message) => {
    ToastAndroid.show(message, ToastAndroid.SHORT);
};

const initialState = {
    expenseItems: [
        {
            id: uuid.v4(),
            desc: 'Grocery',
            cat: 'Housing',
            amnt: '30.00',
            isHidden: false,
            dateAdded: new Date().toDateString()
        }
    ],
    monthlyTotal: 30,
    externalCallTest: [],
    lastUpdated: new Date().toDateString(),
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_ALL_EXPENSES_REQUEST_SUCCESS: {
            const { expenseItems } = state.expenseItems;            
            return { expenseItems };
        }
        
        case STORE_EXPENSES_REQUEST_SUCCESS: {
            const { desc, cat, amnt } = action.payload;
            return {
                ...state,
                expenseItems: [...state.expenseItems, 
                    {
                        id: uuid.v4(),
                        desc: desc,
                        cat: cat,
                        isHidden: false,
                        amnt: parseFloat(amnt).toFixed(2),
                        dateAdded: new Date().toDateString()
                    }
                ],
                monthlyTotal: parseFloat(state.monthlyTotal) + parseFloat(amnt),
                lastUpdated: new Date().toDateString()
            };
        }

        case REMOVE_EXPENSES_REQUEST_SUCCESS: {
            const { id, amnt } = action.payload.item;
            let originalList = [...state.expenseItems];
            let indexToRmove = originalList.findIndex(x=>{return x.id == id});
            
            if (indexToRmove !== -1 && originalList.length > 1){
                showNewExpenseToast("Expenses deleted..");
                return {
                        ...state,
                        expenseItems: [...state.expenseItems.slice(0, indexToRmove), ...state.expenseItems.slice(indexToRmove + 1)],
                        monthlyTotal: parseFloat(state.monthlyTotal) - parseFloat(amnt),
                        lastUpdated: new Date().toDateString()
                };
            } else {
                showNewExpenseToast("Cannot delete the only expenses left..");
            }
        }

        case EXTERNAL_TEST_REQUEST_SUCCESS: {
            showNewExpenseToast("External API success "+action.serverExpensesList.length+" item(s) fetched");
            return {
                ...state,
                externalCallTest: action.serverExpensesList
            };
        }

        case EXTERNAL_TEST_REQUEST_FAILURE: {
            showNewExpenseToast("External API call failed");
            return {
                ...state,
                externalCallTest: []
            };
        }

        default:
            return state;
    }
};

export { reducer };