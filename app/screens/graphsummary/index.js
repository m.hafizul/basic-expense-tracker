import * as React from 'react';
import { Dimensions } from 'react-native';
import { Center, Text, Box, ScrollView, Button, VStack } from 'native-base';
import { connect } from "react-redux";
import {
  LineChart
} from "react-native-chart-kit";

const mapStateToProps = (state, props) => {
  const { expenseItems } = state.expense;
  return { expenseItems };
};

const mapDispatchToProps = (dispatch, props) => ({ });

const GraphScreen = ({ expenseItems, navigation }) => {
  return (
    <ScrollView>
       <Center flex={1} safeArea>
      <Box w="100%" h="100%" justifyContent="space-between">
        <VStack space={2}>
          <Text
            alignSelf={{
              base: "center",
              md: "flex-start",
            }}
            fontSize="2xl"
            paddingTop={10}
          >
            Basic Summary
          </Text>
          <Center>
            <LineChart
              data={{
                labels: ["Housing", "Food", "Trans.", "Cell", "Insurance", "Debt", "Retail"],
                datasets: [
                  {
                    data: [
                      expenseItems.filter(x => { return x.cat == 'Housing' }).map(x => { return parseFloat(x.amnt) }).reduce((a, b) => a + b, 0),
                      expenseItems.filter(x => { return x.cat == 'Food' }).map(x => { return parseFloat(x.amnt) }).reduce((a, b) => a + b, 0),
                      expenseItems.filter(x => { return x.cat == 'Transportation' }).map(x => { return parseFloat(x.amnt) }).reduce((a, b) => a + b, 0),
                      expenseItems.filter(x => { return x.cat == 'Cellphone' }).map(x => { return parseFloat(x.amnt) }).reduce((a, b) => a + b, 0),
                      expenseItems.filter(x => { return x.cat == 'Insurance' }).map(x => { return parseFloat(x.amnt) }).reduce((a, b) => a + b, 0),
                      expenseItems.filter(x => { return x.cat == 'Debt' }).map(x => { return parseFloat(x.amnt) }).reduce((a, b) => a + b, 0),
                      expenseItems.filter(x => { return x.cat == 'Retail' }).map(x => { return parseFloat(x.amnt) }).reduce((a, b) => a + b, 0)
                    ]
                  }
                ]
              }}
              width={Dimensions.get("window").width - 20} // from react-native
              height={220}
              yAxisLabel="$"
              yAxisSuffix=""
              yAxisInterval={1} // optional, defaults to 1
              chartConfig={{
                backgroundColor: "#e26a00",
                backgroundGradientFrom: "#fb8c00",
                backgroundGradientTo: "#ffa726",
                decimalPlaces: 2, // optional, defaults to 2dp
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                style: {
                  borderRadius: 16
                },
                propsForDots: {
                  r: "6",
                  strokeWidth: "2",
                  stroke: "#ffa726"
                }
              }}
              bezier
              style={{
                marginVertical: 8,
                borderRadius: 16
              }}
            />
            <Button onPress={() => navigation.goBack()}>Go Back</Button>
          </Center>
        </VStack>
      </Box>
    </Center>
    </ScrollView>
  
  );
};

const Graph = connect(
  mapStateToProps,
  mapDispatchToProps,
)(GraphScreen);

export { Graph };