import * as React from 'react';
import { useState } from "react";
import { Center, Text, Box, Button, ScrollView, VStack } from 'native-base';
import { connect } from "react-redux";
import { removeExpenses } from '../../models/expense/actions';
import ExpenseItem from './components/ExpenseItem';
import HeaderCard from './components/HeaderCard';
import Filter from './components/Filter';

const mapStateToProps = (state, props) => {
  const { expenseItems, monthlyTotal } = state.expense;
  return { expenseItems, monthlyTotal };
};

const mapDispatchToProps = (dispatch, props) => ({
  removeExpenses: (expense) => dispatch(removeExpenses(expense))
});


const SummaryScreen = ({ navigation, monthlyTotal, removeExpenses, expenseItems }) => {

  const [stateExpenseItems, setStateExpenseItems] = useState(expenseItems);

  const onDeleteClick = (selectedExpenseObject) => {
    if (shouldAllowDeletion()) {
      let updatedExpenseItems = stateExpenseItems.filter(x => x.id !== selectedExpenseObject.item.id);
      setStateExpenseItems(updatedExpenseItems);
    }
    removeExpenses(selectedExpenseObject);
  };

  const shouldAllowDeletion = () => {
    return stateExpenseItems.length > 1;
  }

  const onCategoryFilterSelected = (value) => {
    if(value.category) {
      const selectedCat = value.category;
      let updatedExpenseItems = stateExpenseItems.filter(x=>{return x.id}); 
      updatedExpenseItems.forEach(expenseItem => {
        expenseItem.isHidden = expenseItem.cat === selectedCat ? false : true;
      });
      setStateExpenseItems(updatedExpenseItems);
    }
  };

  return (
    <Center flex={1} safeArea>
      {stateExpenseItems && stateExpenseItems.length > 0 ?
        <Box w="85%" h="90%">
          <HeaderCard navigation={navigation} monthlyTotal={monthlyTotal}></HeaderCard>
          <Filter onSelected={onCategoryFilterSelected}></Filter>
          <ScrollView>
            {stateExpenseItems.map((item, index) => {
              if(!item.isHidden) {
                return (
                  <ExpenseItem key={item.id} item={item} onDeleteClick={onDeleteClick}></ExpenseItem>
                );
              } else {
                <VStack>
                  <Text fontSize="lg">No Expense Item of the category</Text>
                </VStack>
              }
            })}
          </ScrollView>
        </Box>
        :
        <VStack>
          <Text fontSize="lg">Please add expenses to view summary</Text>
          <Button onPress={() => navigation.goBack()}>Go Back</Button>
        </VStack>
      }
    </Center>
  );
};

const Summary = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SummaryScreen);

export { Summary };