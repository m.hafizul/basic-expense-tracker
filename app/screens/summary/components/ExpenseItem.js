import * as React from 'react';
import { Text, Box, HStack, VStack } from 'native-base';

export default ExpenseItem = ({ item, onDeleteClick }) => {
  return (
    <Box
      key={item.id}
      px={5} py={2} rounded="md" my={2}
      bg="tertiary.600"
      p={4}
      _text={{
        fontSize: "md",
        fontWeight: "bold",
        color: "white",
      }}
      borderRadius={4}
    >
      <VStack>
        <HStack>
          <Text color="white" fontSize="lg">{item.desc} </Text>
          <Text color="white" bold fontSize="xl"> ${item.amnt} </Text>
        </HStack>
        <Text textAlign="left" color="white" fontSize="xs" bold> {item.cat} </Text>
        <Text textAlign="left" color="white" fontSize="xs" italic> {item.dateAdded} </Text>
        <Text textAlign="right" onPress={() => onDeleteClick({ item })} justifyContent="flex-end" color="white" fontSize="sm">Delete</Text>
      </VStack>
    </Box>
  );
};