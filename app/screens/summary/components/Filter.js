import * as React from 'react';
import { CheckIcon, Select } from 'native-base';

export default Filter = ({ onSelected }) => {
    return (
        <Select
            minWidth={200}
            accessibilityLabel="Select your expense type"
            placeholder="Select your expense type"
            onValueChange={(value) => onSelected({ category: value })}
            _selectedItem={{
                bg: "teal.600",
                endIcon: <CheckIcon size={5} />,
            }}
            mt={1}
        >
            <Select.Item label="Housing" value="Housing" />
            <Select.Item label="Food" value="Food" />
            <Select.Item label="Transportation" value="Transportation" />
            <Select.Item label="Cellphone" value="Cellphone" />
            <Select.Item label="Insurance" value="Insurance" />
            <Select.Item label="Debt" value="Debt" />
            <Select.Item label="Retail" value="Retail" />
        </Select>
    );
};