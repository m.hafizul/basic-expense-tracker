import * as React from 'react';
import { Text, Box, HStack, Button, VStack } from 'native-base';

export default HeaderCard = ({ navigation, monthlyTotal }) => {
    return (
        <HStack>
            <Box justifyContent="space-between"
                bg="primary.600"
                py={4}
                px={3}
                rounded="md"
                alignSelf="center"
                width={375}
                maxWidth="100%">
                <VStack space={2}>
                    <Text fontSize="sm" color="white" italic>
                        Total Monthly Expenditure
                    </Text>
                    <Text color="white" fontSize="3xl">
                        ${monthlyTotal}
                    </Text>
                    <Button width="100%" onPress={() => navigation.navigate('Graph')}>Show Graph</Button>
                    <Button onPress={() => navigation.goBack()}>Go Back</Button>
                </VStack>
            </Box>
        </HStack>
    );
};