import * as React from 'react';
import { Center, Button, Stack, Modal, Text, Heading } from 'native-base';
import { useState } from "react";
import { connect } from "react-redux";
import { addNewExpenses, testApiCall } from '../../models/expense/actions';
import { ExpenseForm } from '../../components/expenseForm';

const mapStateToProps = (state, props) => {
  const { expenseItems, lastUpdated } = state.expense;
  return { lastUpdated, expenseItems };
};

const mapDispatchToProps = (dispatch, props) => ({
  addNewExpenses: (formData) => dispatch(addNewExpenses(formData)),
  testApiCall: () => dispatch(testApiCall())
});

const HomeScreen = ({ navigation, testApiCall }) => {
  const [showModal, setShowModal] = useState(false);

  const handleTestCall = () => {
    testApiCall();
  };

  return (
    <>
      <Center flex={1}>
        <Stack
          direction={{
            base: "column",
            md: "row",
          }}
          space={4}
        >
          <Heading size="lg">
            Expense
            Tracker
          </Heading>

          <Text fontSize="lg">
            Demo app to demonstrate react-redux, redux-saga, react-native, as well as other UI library constructure.
          </Text>
          <Button onPress={() => setShowModal(true)}>Add Expenses</Button>
          <Button bg="blue.600" onPress={() => navigation.navigate('Summary')}>Summary</Button>
          <Button bg="blue.600" onPress={() => handleTestCall()}>External Call</Button>
        </Stack>
      </Center>

      <Modal isOpen={showModal} onClose={() => { setShowModal(false) }}>
        <ExpenseForm/>
      </Modal>
    </>
  );
};

const Home = connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeScreen);

export { Home };