import * as React from 'react';
import { Button, Modal, VStack, Input, FormControl, Select, CheckIcon } from 'native-base';
import { useState } from "react";
import { connect } from "react-redux";
import { ToastAndroid } from "react-native";
import { addNewExpenses, testApiCall } from '../models/expense/actions';

const mapStateToProps = (state, props) => {
    const { expenseItems, lastUpdated } = state.expense;
    return { lastUpdated, expenseItems };
};

const mapDispatchToProps = (dispatch, props) => ({
    addNewExpenses: (formData) => dispatch(addNewExpenses(formData)),
    testApiCall: () => dispatch(testApiCall())
});

const showNewExpenseToast = (message) => {
    ToastAndroid.show(message, ToastAndroid.SHORT);
};

const ExpenseFormScreen = ({ addNewExpenses }) => {
    const [formData, setData] = useState({});
    const [errors, setErrors] = useState({});

    const validateInputForm = () => {

        let isDescriptionPassed = formData.name && formData.name != '';
        let isAmountPassed = parseFloat(formData.amount) > 0 && !isNaN(parseFloat(formData.amount));
        let isCategoryPassed = formData.category && formData.category != '';

        setErrors({
            ...errors,
            amount: isAmountPassed ? false : true,
            name: isDescriptionPassed ? false : true,
            category: isCategoryPassed ? false : true
        });

        return isAmountPassed && isCategoryPassed && isDescriptionPassed;
    };

    const onSubmit = () => {
        if (validateInputForm()) {
            addNewExpenses(formData);
            showNewExpenseToast('New expense added..');
        }
    };

    return (
        <Modal.Content maxWidth="400px">
            <Modal.CloseButton />
            <Modal.Header>Add New Expense</Modal.Header>
            <Modal.Body>
                <VStack width="90%" mx={3}>
                    <FormControl isRequired isInvalid={errors?.name}>
                        {/* Input One */}
                        <FormControl.Label _text={{ bold: true }}>Description</FormControl.Label>
                        <Input
                            placeholder="Grocery"
                            onChangeText={(value) => setData({ ...formData, name: value })}
                        />
                        {
                            errors?.name ?
                                <FormControl.ErrorMessage _text={{ fontSize: 'xs', color: 'error.500', fontWeight: 500 }}>Please enter a descriptive expense name</FormControl.ErrorMessage>
                                :
                                ''
                        }
                    </FormControl>
                    <FormControl isRequired isInvalid={errors?.amount == true}>
                        {/* Input Two */}
                        <FormControl.Label _text={{ bold: true }}>Amount ($)</FormControl.Label>
                        <Input
                            placeholder="Amount"
                            onChangeText={(value) => setData({ ...formData, amount: value })}
                        />
                        {
                            errors?.amount ?
                                <FormControl.ErrorMessage _text={{ fontSize: 'xs', color: 'error.500', fontWeight: 500 }}>Please enter a valid expense amount</FormControl.ErrorMessage>
                                :
                                ''
                        }
                    </FormControl>
                    <FormControl isRequired isInvalid={errors?.category}>
                        <FormControl.Label _text={{ bold: true }}>Category</FormControl.Label>
                        <Select
                            selectedValue={formData.category}
                            minWidth={200}
                            accessibilityLabel="Select your expense type"
                            placeholder="Select your expense type"
                            onValueChange={(value) => setData({ ...formData, category: value })}
                            _selectedItem={{
                                bg: "teal.600",
                                endIcon: <CheckIcon size={5} />,
                            }}
                            mt={1}
                        >
                            <Select.Item label="Housing" value="Housing" />
                            <Select.Item label="Food" value="Food" />
                            <Select.Item label="Transportation" value="Transportation" />
                            <Select.Item label="Cellphone" value="Cellphone" />
                            <Select.Item label="Insurance" value="Insurance" />
                            <Select.Item label="Debt" value="Debt" />
                            <Select.Item label="Retail" value="Retail" />
                        </Select>
                        {
                            errors?.category ?
                                <FormControl.ErrorMessage _text={{ fontSize: 'xs', color: 'error.500', fontWeight: 500 }}>Please select expense category</FormControl.ErrorMessage>
                                :
                                ''
                        }
                    </FormControl>
                    <Button onPress={onSubmit} mt={5} colorScheme="cyan" variant="ghost">
                        Submit
                    </Button>
                </VStack>
            </Modal.Body>
        </Modal.Content>
    );
};

const ExpenseForm = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ExpenseFormScreen);

export { ExpenseForm };