import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Home } from '../screens/home/index';
import { Summary } from '../screens/summary';
import { Graph } from '../screens/graphsummary';
import * as React from 'react';
import { NativeBaseProvider } from 'native-base';

const NavStack = createNativeStackNavigator();

const Navigation = ({}) => {
    return (
      <NativeBaseProvider>
        <NavigationContainer>
          <NavStack.Navigator screenOptions={{ headerShown: false }}>
              <NavStack.Screen name="Home" component={Home} />
              <NavStack.Screen name="Summary" component={Summary} />
              <NavStack.Screen name="Graph" component={Graph} />
          </NavStack.Navigator>
        </NavigationContainer>
      </NativeBaseProvider>
    );
};

export { Navigation };