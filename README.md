# Basic Expense Tracker

Demo app to demonstrate react-redux, redux-saga, react-navigation, react-native, as well as other UI library constructure.

# Installation

- [1] npm install
- [2] npx react-native run-android


# Environment Tested
- [1] Android Studio 2020.3.1 with ANDROID_SDK, JAVA_HOME, AVD_HOME and all related environment variables setup
- [2] react-native@0.65.1
- [3] Native Base UI library
- [4] npm --version : 6.14.4
- [5] node --version

# Addendum
- iOS is not Tested

